# BACA #

Berikut daftar latihan dasar algoritma yang harus di selesaikan menggunakan PHP dan C. Harap diperhatikan bahwa ini hanya berlaku untuk angkatan 06.

## Selesaikan ini: ##

### 1. Menghitung penjumlahan digit. ###
** Contoh Input: **
``` 
689652345998 
```
** Contoh Output: **
``` 
74 
11
2 
```

** Contoh Input: **
``` 
555566677 
```
** Contoh Output: **
``` 
52 
7 
```

### 2. Mencari apakah bilangan (n) termasuk dalam deret fibonacci. ###
** Contoh Input: **
``` 
13
```
** Contoh Output: **
``` 
Ya, Ini termasuk deret fibonacci 
```

** Contoh Input: **
```
19 
```
** Contoh Output: **
``` 
Tidak, Ini Bukan deret fibonacci 
```

### 3. Konversi ukuran (meter,cm,milli,inchi,kilometer). ###
## Mengenal argc dan argv: ##
** Contoh Antarmuka: **
```
./programkamu --help
Gunakan: ./programkamu [options1] <nilai> [options2] <jenis>
-mt, Konversi meter 
-cm, Konversi centimeter 
-mi, Konversi millimeter
-in, konversi inchi
-km, konversi kilometer
-o, output dari konversi
-h, --help, Petunjuk penggunaan
Example: 
./programku -mt 200 -o centimeter
200m = 20000cm
```
# *Bersambung...* #

